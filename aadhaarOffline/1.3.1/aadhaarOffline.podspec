#
# Be sure to run `pod lib lint aadhaarOffline.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'aadhaarOffline'
  s.version          = '1.3.1'
  s.summary          = 'Extraction of Aadhaar details from file'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/aadhaaroffline'
  s.license          = 'MIT'
  s.author           = { 'ashish' => 'ashish@frslabs.com' }
  s.source           = { :http => 'https://octus-aadhaar-offline-ios.repo.frslabs.space/octus-aadhaar-offline-ios/1.3.1/aadhaarOffline.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'aadhaarOffline.framework'
  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  
  end
